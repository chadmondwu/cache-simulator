/*
 * Direct Mapped Associative Cache 
 * Authors: Chadmond Wu, Shaydon Bodemar
 * 05/5/19
*/

#include "storage.h"
#include "cache_direct.h"
#include <string.h>
#include <stdio.h>
//CACHE_ENTRIES = 16

static int misses = 0;
static int hits = 0;

static struct directCache
{
	unsigned char v_d_flag;
	cache_line cacheBlock;
	unsigned int entryAddr;
} cache[CACHE_ENTRIES];

void cache_direct_init()
{
	for (int i = 0; i < CACHE_ENTRIES; i++)
	{
		cache[i].entryAddr = 0;
		cache[i].v_d_flag = 0;
		memset(cache[i].cacheBlock, 0, sizeof(cache[i].cacheBlock));
	}
	misses = 0;
	hits = 0;
}
int cache_direct_load(memory_address addr)
{
	unsigned char offsetAddr = addr & 0xF;
	unsigned int tagAddr = addr >> 8;
	unsigned int set_index = (addr >> 4) & 0xF;

	unsigned char v_flag = cache[set_index].v_d_flag & 1; 
	unsigned char d_flag = cache[set_index].v_d_flag >> 1;
	unsigned int tagEntry = cache[set_index].entryAddr >> 8;

	if (v_flag && tagEntry == tagAddr)
	{
		hits++;
	}
	else
	{
		misses++;
		if (d_flag)
		{
			storage_store_line(cache[set_index].entryAddr, cache[set_index].cacheBlock);
		}
		storage_load_line(addr, cache[set_index].cacheBlock);
		cache[set_index].entryAddr = addr;
		cache[set_index].v_d_flag = 1; 
	}
	return *(int*)(&cache[set_index].cacheBlock[offsetAddr]);
}
void cache_direct_store(memory_address addr, int value)
{
	unsigned char offsetAddr = addr & 0xF;
	unsigned int tagAddr = addr >> 8;
	unsigned int set_index = (addr >> 4) & 0xF;

	unsigned char v_flag = cache[set_index].v_d_flag & 1; 
	unsigned char d_flag = cache[set_index].v_d_flag >> 1;
	unsigned int tagEntry = cache[set_index].entryAddr >> 8;

	if (v_flag && tagEntry == tagAddr)
	{
		hits++;
	}
	else
	{
		misses++;
		if (d_flag)
		{
			storage_store_line(cache[set_index].entryAddr, cache[set_index].cacheBlock);
		}
		storage_load_line(addr, cache[set_index].cacheBlock);
		cache[set_index].entryAddr = addr;
	}
	cache[set_index].v_d_flag = 3; 
	*(int*)(&cache[set_index].cacheBlock[offsetAddr]) = value;
}
void cache_direct_flush()
{
	for (unsigned int i = 0; i < CACHE_ENTRIES; i++)
	{
		if ((cache[i].v_d_flag >> 1) && (cache[i].v_d_flag & 1))
		{
			storage_store_line(cache[i].entryAddr, cache[i].cacheBlock);
			cache[i].v_d_flag = 1; 
		}
	}
}
void cache_direct_stats()
{
	printf("Direct Cache Stats- Hits: %d, Misses: %d\n", hits, misses);
}