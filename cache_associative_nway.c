/*
 * Nway Set Associative Cache 
 * Authors: Chadmond Wu, Shaydon Bodemar
 * 05/5/19
*/

#include "storage.h"
#include "cache_associative_nway.h"
#include <string.h>
#include <stdio.h>
//CACHE_ENTRIES = 16

static int misses = 0;
static int hits = 0;
static int timer = 0;

static struct nwayCache
{
	unsigned char v_d_flag;
	cache_line cacheBlock;
	unsigned int age;
	memory_address entryAddr;
} cache[8][2];

void cache_associative_nway_init()
{
	hits = 0;
	misses = 0;

	for (int i = 0; i < CACHE_ENTRIES/2; i++)
	{
		for (int j = 0; j < 2; j++)
		{
			cache[i][j].entryAddr = 0;
			cache[i][j].v_d_flag = 0;
			cache[i][j].age = 0;
			memset(cache[i][j].cacheBlock, 0, sizeof(cache[i][j].cacheBlock));
		}
	}
}

int cache_associative_nway_load(memory_address addr)
{
	unsigned int tagAddr = addr >> 7;
	unsigned char offsetAddr = addr & 0xF;
	unsigned char set_index = (addr >> 4) & 7;

	unsigned char indexEntry = 0;
	unsigned char v_flag = 0;
	unsigned char d_flag = 0;
	unsigned int tagEntry = 0;

	timer++;
	while (indexEntry < 2)
	{
		tagEntry = cache[set_index][indexEntry].entryAddr >> 7;
		v_flag = cache[set_index][indexEntry].v_d_flag & 1;
		if (v_flag && tagAddr == tagEntry)
		{
			hits++;
			break;
		}
		indexEntry++;
	}
	if (indexEntry == 2)
	{
		misses++;
		for (int i = 0; i < 2; i++)
		{
			if (!(cache[set_index][i].v_d_flag & 1))
				indexEntry = i;
			else
				indexEntry = 2;
		}
		if (indexEntry == 2)
		{
			unsigned int lru_i = 0;
			unsigned int lru = cache[set_index][lru_i].age;
			for (int j = 0; j < 2; j++)
			{
				if (lru > cache[set_index][j].age)
				{
					lru = cache[set_index][j].age;
					lru_i = j;
				}
				else if (lru == 0)
				{
					lru_i = j;
					break;
				}
			}
			indexEntry = lru_i;

			d_flag = cache[set_index][indexEntry].v_d_flag >> 1;
			if (d_flag)
				storage_store_line(cache[set_index][indexEntry].entryAddr, cache[set_index][indexEntry].cacheBlock);
		}
		storage_load_line(addr, cache[set_index][indexEntry].cacheBlock);
		cache[set_index][indexEntry].v_d_flag = 1;
		cache[set_index][indexEntry].entryAddr = addr;
	}

	cache[set_index][indexEntry].age = timer;
	return *(int *)(&cache[set_index][indexEntry].cacheBlock[offsetAddr]);
}
void cache_associative_nway_store(memory_address addr, int value)
{
	unsigned int tagAddr = addr >> 7;
	unsigned char offsetAddr = addr & 0xF;
	unsigned char set_index = (addr >> 4) & 7;

	unsigned int indexEntry = 0;
	unsigned char v_flag = 0;
	unsigned char d_flag = 0;
	unsigned int tagEntry = cache[set_index][indexEntry].entryAddr >> 7;

	timer++;
	while (indexEntry < 2)
	{
		tagEntry = cache[set_index][indexEntry].entryAddr >> 7;
		v_flag = cache[set_index][indexEntry].v_d_flag & 1;
		if (v_flag && tagAddr == tagEntry)
		{
			hits++;
			break;
		}
		indexEntry++;
	}
	if (indexEntry == 2)
	{
		misses++;
		for (int i = 0; i < 2; i++)
		{
			if (!cache[set_index][i].v_d_flag & 1)
				indexEntry = i;
			else
				indexEntry = 2;
		}
		if (indexEntry == 2)
		{
			unsigned int lru_i = 0;
			unsigned int lru = cache[set_index][lru_i].age;
			for (int j = 0; j < 2; j++)
			{
				if (lru > cache[set_index][j].age)
				{
					lru = cache[set_index][j].age;
					lru_i = j;
				}
				else if (lru == 0)
				{
					lru_i = j;
					break;
				}
			}
			indexEntry = lru_i;

			d_flag = cache[set_index][indexEntry].v_d_flag >> 1;
			if (d_flag)
				storage_store_line(cache[set_index][indexEntry].entryAddr, cache[set_index][indexEntry].cacheBlock);
		}
		storage_load_line(addr, cache[set_index][indexEntry].cacheBlock);
		cache[set_index][indexEntry].v_d_flag = 1;
		cache[set_index][indexEntry].entryAddr = addr;
	}
	cache[set_index][indexEntry].age = timer;
	cache[set_index][indexEntry].v_d_flag = 3;
	*(int *)(&cache[set_index][indexEntry].cacheBlock[offsetAddr]) = value;
}
void cache_associative_nway_flush()
{
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 2; j++)
		{
			if ((cache[i][j].v_d_flag >> 1) && (cache[i][j].v_d_flag & 1))
			{
				storage_store_line(cache[i][j].entryAddr, cache[i][j].cacheBlock);
				cache[i][j].v_d_flag = 1;
			}
		}
	}
}
void cache_associative_nway_stats()
{
	printf("Nway Associative Cache Stats- Hits: %d, Misses: %d\n", hits, misses);
}