/*
 * Fully Associative Associative Cache 
 * Authors: Chadmond Wu, Shaydon Bodemar
 * 05/5/19
*/

#include "storage.h"
#include "cache_associative_full.h"
#include <string.h>
#include <stdio.h>
//CACHE_ENTRIES = 16

static int misses = 0;
static int hits = 0;
static int timer = 0;

static struct fullCache
{
	unsigned char v_d_flag;
	cache_line cacheBlock;
	unsigned int age;
	memory_address entryAddr;
} cache[CACHE_ENTRIES];

static int check_valid()
{
	unsigned int validEntry = 0;
	for (int i = 0; i < CACHE_ENTRIES; i++)
	{
		validEntry = cache[i].v_d_flag & 0x1;
		if (!validEntry)
			return i;
	}
	return CACHE_ENTRIES;
}
void cache_associative_full_init()
{
	hits = 0;
	misses = 0;

	for (int i = 0; i < CACHE_ENTRIES; i++)
	{
		cache[i].v_d_flag = 0;
		cache[i].age = 0;
		cache[i].entryAddr = 0;
		memset(cache[i].cacheBlock, 0, sizeof(cache[i].cacheBlock));
	}
}
int cache_associative_full_load(memory_address addr)
{
	unsigned int tagAddr = addr >> 4;
	unsigned char offsetAddr = addr & 0xF;

	unsigned int indexEntry = 0;
	unsigned char v_flag = 0;
	unsigned char d_flag = 0;
	unsigned int tagEntry = 0;

	timer++;
	while (indexEntry < CACHE_ENTRIES)
	{
		tagEntry = cache[indexEntry].entryAddr >> 4;
		v_flag = cache[indexEntry].v_d_flag & 1;
		if (v_flag && tagAddr == tagEntry)
		{
			hits++;
			break;
		}
		indexEntry++;
	}
	if (indexEntry == CACHE_ENTRIES)
	{
		misses++;
		indexEntry = check_valid();

		if (indexEntry == CACHE_ENTRIES)
		{
			unsigned int lru_i = 0;
			unsigned int lru = cache[0].age;

			for (int j = 0; j < CACHE_ENTRIES; j++)
			{
				if (lru > cache[j].age)
				{
					lru = cache[j].age;
					lru_i = j;
				}
				else if (lru == 0)
				{
					lru_i = j;
					break;
				}
			}
			indexEntry = lru_i;

			d_flag = cache[indexEntry].v_d_flag >> 1;
			if (d_flag)
				storage_store_line(cache[indexEntry].entryAddr, cache[indexEntry].cacheBlock);
		}
		storage_load_line(addr, cache[indexEntry].cacheBlock);
		cache[indexEntry].v_d_flag = 1;
		cache[indexEntry].entryAddr = addr;
	}
	cache[indexEntry].age = timer;
	return *(int *)(&cache[indexEntry].cacheBlock[offsetAddr]);
}
void cache_associative_full_store(memory_address addr, int value)
{
	unsigned int tagAddr = addr >> 4;
	unsigned char offsetAddr = addr & 0xF;

	unsigned int indexEntry = 0;
	unsigned char v_flag = 0;
	unsigned char d_flag = 0;
	unsigned int tagEntry = cache[indexEntry].entryAddr >> 4;

	timer++;
	while (indexEntry < CACHE_ENTRIES)
	{
		tagEntry = cache[indexEntry].entryAddr >> 4;
		v_flag = cache[indexEntry].v_d_flag & 1;
		if (v_flag && tagAddr == tagEntry)
		{
			hits++;
			break;
		}
		indexEntry++;
	}
	if (indexEntry == CACHE_ENTRIES)
	{
		misses++;
		indexEntry = check_valid();

		if (indexEntry == CACHE_ENTRIES)
		{
			unsigned int lru_i = 0;
			unsigned int lru = cache[0].age;

			for (int j = 0; j < CACHE_ENTRIES; j++)
			{
				if (lru > cache[j].age)
				{
					lru = cache[j].age;
					lru_i = j;
				}
				else if (lru == 0)
				{
					lru_i = j;
					break;
				}
			}
			indexEntry = lru_i;

			d_flag = cache[indexEntry].v_d_flag >> 1;
			if (d_flag)
				storage_store_line(cache[indexEntry].entryAddr, cache[indexEntry].cacheBlock);
		}
		storage_load_line(addr, cache[indexEntry].cacheBlock);
		cache[indexEntry].v_d_flag = 1;
		cache[indexEntry].entryAddr = addr;
	}
	cache[indexEntry].age = timer;
	cache[indexEntry].v_d_flag = 3;
	*(int *)(&cache[indexEntry].cacheBlock[offsetAddr]) = value;
}
void cache_associative_full_flush()
{
	for (int i = 0; i < CACHE_ENTRIES; i++)
	{
		if ((cache[i].v_d_flag >> 1) && (cache[i].v_d_flag & 1))
		{
			storage_store_line(cache[i].entryAddr, cache[i].cacheBlock);
			cache[i].v_d_flag = 1;
		}
	}
}
void cache_associative_full_stats()
{
	printf("Fully Associative Cache Stats- Hits: %d, Misses: %d \n", hits, misses);
}